package bzh.lautre.bookshelf.bnf.ws.model

class AuthorSearchResult(var bnfId: String? = null,
                         var lastname: String? = null,
                         var firstname: String? = null,
                         var role: Set<String>? = null) {

    val name: String
        get() = this.firstname + " " + this.lastname

}
