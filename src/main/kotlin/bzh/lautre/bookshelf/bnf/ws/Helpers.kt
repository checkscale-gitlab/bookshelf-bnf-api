package bzh.lautre.bookshelf.bnf.ws

import bzh.lautre.bookshelf.bnf.ws.model.AuthorSearchResult
import org.apache.commons.lang3.tuple.Pair
import org.w3c.dom.NodeList

import java.util.ArrayList
import kotlin.reflect.KMutableProperty
import kotlin.reflect.KMutableProperty.Setter
import kotlin.reflect.full.memberProperties

internal object Helpers {

    @Throws(ClassNotFoundException::class)
    fun getSetter(fieldName: String): Setter<*> {
        val property = AuthorSearchResult::class.memberProperties.find { it.name == fieldName }
        if (property is KMutableProperty<*>) {
            return property.setter
        } else {
            throw ClassNotFoundException()
        }
    }

    fun getXpathExpByPair(pair: Pair<String, String>): String {
        return "//datafield[@tag='" + pair.left + "']/subfield[@code='" + pair.right + "']"
    }

    fun mapToContentList(nodeList: NodeList): List<String> {
        val stringList = ArrayList<String>()
        for (i in 0 until nodeList.length) {
            stringList.add(nodeList.item(i).textContent)
        }
        return stringList
    }
}
