package bzh.lautre.bookshelf.bnf.ws

import org.apache.commons.lang3.tuple.Pair
import org.springframework.stereotype.Component
import org.w3c.dom.Document
import org.w3c.dom.NodeList

import javax.xml.xpath.XPath
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathExpressionException
import javax.xml.xpath.XPathFactory
import java.util.ArrayList
import java.util.concurrent.atomic.AtomicReference

import bzh.lautre.bookshelf.bnf.ws.Helpers.getXpathExpByPair

@Component
class XPathAnalyser {

    private val xPath: XPath = XPathFactory.newInstance().newXPath()

    internal fun getNodeList(document: Document, pairList: List<Pair<String, String>>): NodeList {
        val values = AtomicReference<NodeList>()

        pairList.stream()
                .map(Helpers::getXpathExpByPair)
                .reduce { s, s2 -> "$s | $s2" }
                .ifPresent { expression ->
                    try {
                        if (values.get() == null) {
                            values.set(this.xPath.compile(expression).evaluate(document, XPathConstants.NODESET) as NodeList)
                        }
                    } catch (e: XPathExpressionException) {
                        e.printStackTrace()
                    }
                }
        return values.get()
    }

    internal fun getValueStringByPair(document: Document, pairList: List<Pair<String, String>>): String {
        val expressions = ArrayList<String>()
        pairList.forEach { pair -> expressions.add(getXpathExpByPair(pair)) }
        return getValueString(document, expressions)
    }

    internal fun getValueString(document: Document, expressions: List<String>): String {
        val values = AtomicReference("")
        expressions.forEach { expression ->
            try {
                if (values.get() == null || "" == values.get()) {
                    values.set(this.xPath.compile(expression).evaluate(document, XPathConstants.STRING) as String)
                }
            } catch (e: XPathExpressionException) {
                e.printStackTrace()
            }
        }
        return values.get()
    }
}
