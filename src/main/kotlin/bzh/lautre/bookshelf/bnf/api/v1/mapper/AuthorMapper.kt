package bzh.lautre.bookshelf.bnf.api.v1.mapper

import bzh.lautre.bookshelf.bnf.api.v1.model.AuthorDTO
import bzh.lautre.bookshelf.bnf.ws.model.AuthorSearchResult
import org.mapstruct.Mapper

@Mapper(componentModel = "spring")
interface AuthorMapper {

    fun map(authorSearchResult: AuthorSearchResult): AuthorDTO
}
