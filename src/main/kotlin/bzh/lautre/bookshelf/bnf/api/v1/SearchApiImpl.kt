package bzh.lautre.bookshelf.bnf.api.v1

import bzh.lautre.bookshelf.bnf.api.v1.mapper.BookMapper
import bzh.lautre.bookshelf.bnf.api.v1.model.BookDTO
import bzh.lautre.bookshelf.bnf.ws.BnfSruApi
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.RestController

@Validated
@RestController
@Api(tags = ["search"])
class SearchApiImpl @Autowired
constructor(
    private val bnfSruApi: BnfSruApi,
    private val bookMapper: BookMapper
) : SearchApi {

    override fun findBooksByISBN(isbn: String): ResponseEntity<BookDTO> {
        return ResponseEntity.of(this.bnfSruApi.getBookDataByIsbn(sanitizeIsbn(isbn)).map(bookMapper::map))
    }

    private fun sanitizeIsbn(isbn: String) = isbn
        .replace("-", "")
        .replace(" ", "")
        .trim { it <= ' ' }

}

