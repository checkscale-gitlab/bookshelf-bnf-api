package bzh.lautre.bookshelf.bnf.api.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.*
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

@Configuration
@EnableSwagger2
class SwaggerConfig (
    internal val swaggerDocumentationConfig: SwaggerDocumentationConfig,
) {

    @Bean
    fun api(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
            .groupName("Generated")
                .select()
                .apis(RequestHandlerSelectors.basePackage("bzh.lautre.bookshelf.bnf"))
                .build()
            .pathMapping("/")
            .tags(Tag("search", "Search book with ISBN"))
                .directModelSubstitute(java.time.LocalDate::class.java, java.sql.Date::class.java)
                .directModelSubstitute(java.time.OffsetDateTime::class.java, java.util.Date::class.java)
                .apiInfo(apiInfo())
    }

    private fun apiInfo(): ApiInfo {
        return this.swaggerDocumentationConfig.apiInfo()
    }
}
