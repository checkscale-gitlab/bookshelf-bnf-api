package bzh.lautre.bookshelf.bnf.api.v1.mapper

import bzh.lautre.bookshelf.bnf.api.v1.model.BookDTO
import bzh.lautre.bookshelf.bnf.ws.model.BookSearchResult
import org.mapstruct.Mapper

@Mapper(componentModel = "spring")
interface BookMapper {

    fun map(bookSearchResult: BookSearchResult): BookDTO
}
